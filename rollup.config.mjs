import fs from "fs-extra";
import path from "path";
import terser from "@rollup/plugin-terser";

import { Component } from "totates";
import data from "./component.json" assert { type: "json" };
const component = new Component(data);

const { destDir, manifest } = component;
const lodashDir = "node_modules/lodash-es/";

const config = [];

for (const filename of fs.readdirSync(lodashDir)) {
    const parsed = path.parse(filename);
    if (parsed.ext === ".js") {
        config.push({
            input: path.join(lodashDir, filename),
            output: {
                file: path.join(component.destDir, filename),
                format: "esm",
                paths: id => id,
            },
            makeAbsoluteExternalsRelative: false,
            external: id => true,
            plugins: [
                component.resource(parsed.name, "application/javascript"),
                process.env.BUILD === "production" && terser()
            ]
        });
    }
}

config.push(component.getConfig());

export default config;
